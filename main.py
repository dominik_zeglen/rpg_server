from cmdPrompt import commandPrompt
import socket, threading

#_IP = "127.0.0.1"
#_IP = "209.141.38.87"
_IP = ''
_PORT = 8550
_BUFFER_SIZE = 1024

def clientThread(conn, addr, id):
		print "Connected:", addr[0]
		
		while True:
			data = conn.recv(_BUFFER_SIZE)
			if not data:
				print "Closed connection"
				conn.close()
				#remove from clientThreads
				break
				
			if data == "ping":
				conn.send("ping")
			else:
				print "from", addr[0], ":", data
				conn.send("ACK")
		
		print "Closing #%d thread" % id

def serverTCP():
	clientThreads = []
	
	while True:
		s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
		s.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1) 
		s.bind((_IP, _PORT))
		s.listen(1)
		
		conn, addr = s.accept()
		print "Incoming connection"
		
		newClientThread = threading.Thread(target=clientThread, args=(conn, addr, len(clientThreads)))
		clientThreads.append(newClientThread)
		newClientThread.start()

serverThread = threading.Thread(target=serverTCP)
serverThread.start()

commandPromptThread = threading.Thread(target=commandPrompt)
commandPromptThread.start()
